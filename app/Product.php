<?php
/*
|--------------------------------------------------------------------------
| Product Model
|--------------------------------------------------------------------------
|
| Product Modal contains the Product details and all basic methord.
|
|
*/
namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	/**
        @OA\Post(
            path="/products",tags={"Product"},summary="Products responce",operationId="products",description= "Return products details",

			@OA\Response(response=Product Modal collection,
        )
     */

    /*********** function for product list *********/
	public static function getProductModel($orderby, $order)
 	{
  		$orderby  = $orderby ?? 'updated_at';
  		$order    = $order ?? 'desc';
		
		$q        = Product::select('*');

		$count = '';    
		$q->orderBy($orderby, $order);

		$response = $q->get();

		return $response;
	}
}
