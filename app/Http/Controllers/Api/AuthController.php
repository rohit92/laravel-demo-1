<?php
/*
|--------------------------------------------------------------------------
| Auth Controller
|--------------------------------------------------------------------------
|
| Auth controller contains the APIs related to Authentication Process.
|
*/

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPassword;
use App\Mail\ResendForgotOtp;
use App\Mail\ResendRegisterOtp;
use Carbon\Carbon;
use DB;
use URL;
use Validator;
use Illuminate\Support\Facades\Hash;
use Response;
use Illuminate\Contracts\Auth\Authenticatable;

class AuthController extends Controller
{
    

    /**
        @OA\Post(
            path="/login",tags={"User"},summary="User login",operationId="UserLogin",description= "Returns the success message,token and Details of the user. Returns the error message if validation error or any other error occurred.",

            @OA\Parameter(name="email",in="query",required=true,
            @OA\Schema(type="string")),
            @OA\Parameter(name="password",in="query",required=true,
            @OA\Schema(type="string")),

            @OA\Response(response=200,description="Login successfully",
            @OA\MediaType(mediaType="application/json")),
            @OA\Response(response=422,description="Invalid Password"),
            @OA\Response(response=422,description="Account not found with this email "),
            @OA\Response(response=500,description="Please try again later"),
        )
     */

    /*********** function for Login *********/

    public function login (Request $request) {
    try{

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string',
            ]);

            if ($validator->fails())
            {
                return response(['errors'=>$validator->errors()], 422);
            }
            $user = User::where('email', $request->email)->first();
            if ($user) {
                if (Hash::check($request->password, $user->password)) {

                    $user->token = $user->createToken('passport-demo')->accessToken;

                    $response = ['message'=> 'Login successfully','response' => $user];

                    return response($response, 200);
                } else {
                    return response(["message" => "Invalid Password"], 422);
                }
            } else {
                return response(["message" =>'Account not found with this email'], 422);
            }
        }catch (Exception $e){
            return response(["message" =>'Please try again later'], 500);
        }
    }
}



